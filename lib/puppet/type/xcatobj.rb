require 'puppet/resource_api'

Puppet::ResourceApi.register_type(
  name: 'xcatobj',
  docs: <<-EOS,
@summary a xcatobj type
@example
xcatobj { 'foo':
  ensure => 'present',
}

This type provides Puppet with the capabilities to manage xCAT database objects

If your type uses autorequires, please document as shown below, else delete
these lines.
**Autorequires**:
* `Package[foo]`
EOS
  features: [],
  attributes: {
    ensure: {
      type:    'Enum[present, absent]',
      desc:    'Whether this resource should be present or absent on the target system.',
      default: 'present',
    },
    name: {
      type:      'String',
      desc:      'The name of the resource you want to manage.',
      behaviour: :namevar,
    },
    type: {
      type:      'String',
      desc:      'The xCAT database object type',
    },
  },
)
