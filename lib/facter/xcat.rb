# xcat_version.rb
# $ export FACTERLIB=/vagrant/modules/xcat/lib/facter

Facter.add('xcat') do
  confine :kernel => :Linux
  confine do
    File.exist?('/opt/xcat/bin/lsxcatd')
  end

  setcode do
    lsxcatd_hash = {}
    # Default values
    if File.exist?('/etc/xcat/cfgloc')
      lines = File.readlines('/etc/xcat/cfgloc')
      lsxcatd_hash['dbengine'] = 'Pg' if lines.grep(/^Pg:/).any?
    else
      lsxcatd_hash['dbengine'] = 'SQlite'
    end
    lsxcatd_hash['status'] = 'inactive'

    lsxcatd = Facter::Core::Execution.execute('/opt/xcat/bin/lsxcatd -a', options = { :on_fail => :raise })
    if lsxcatd =~ %r{Unable to open socket connection to xcatd daemon on}
      begin
        File.open("/tmp/xcat.facter.state","rb") {|f| lsxcatd = Marshal.load(f)}
      rescue
      end
    else
      File.open("/tmp/xcat.facter.state","wb") {|f| Marshal.dump(lsxcatd, f)}
      lsxcatd_hash['status'] = 'active'
    end

    lsxcatd_hash['version']   = lsxcatd.match('Version (\S*)')[1]        unless lsxcatd !~ %r{Version }
    lsxcatd_hash['node_type'] = lsxcatd.match('This is a (\S*) Node')[1] unless lsxcatd !~ %r{This is a \S* Node}
    lsxcatd.scan(/^(\S+?)=(\S*)/m) {|k,v| lsxcatd_hash[k] = v}

    lsxcatd_hash
  end
end
