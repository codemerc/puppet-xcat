# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include xcat::server
class xcat::server (
  String $dbengine,
  String $dbadmin,
  String $dbname,
  String $dbhost,
  String $dbuser,
  String $dbpassword,
  String $service_name,
         $site_master      = undef,
  String $site_nameservers = "${facts['ipaddress']}",
) {
  $site_master_ip = $site_master ? {
    Stdlib::IP::Address => $site_master,
    undef               => "${facts['ipaddress']}",
    default             => inline_template("<%= scope.lookupvar('::ipaddress_${site_master}') -%>")
  }

  # Sanity checks
  unless $facts['memory']['system']['total_bytes'] > 1073741824 {
    warning('Recommended minimum memory is 4GB to run xCAT')
  }
  class { 'selinux':
    mode => 'disabled',
  }

  include xcat
  contain xcat::server::service
  contain xcat::server::install

  if $xcat::server::dbengine == 'Pg' {
    include xcat::server::postgresql
  }

  #if $facts['xcat'] != undef and $facts['xcat']['version'] != undef and versioncmp($xcat::version, $facts['xcat']['version']) == 1 {
  #  include xcat::server::upgrade
  #}
}
