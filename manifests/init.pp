# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include xcat
class xcat (
  String $version,
  String $site_master_hostname,
  String $site_master = "${site_master_hostname}.${facts['domain']}",
) {
  yumrepo { 'xcat-core':
    ensure   => 'present',
    baseurl  => "http://xcat.org/files/xcat/repos/yum/${xcat::version}/xcat-core",
    enabled  => '1',
    gpgcheck => '1',
    gpgkey   => "http://xcat.org/files/xcat/repos/yum/${xcat::version}/xcat-core/repodata/repomd.xml.key",
    name     => 'xcat-core',
  }
  yumrepo { 'xcat-dep':
    ensure   => 'present',
    baseurl  => "http://xcat.org/files/xcat/repos/yum/${xcat::version}/xcat-dep/rh${facts['operatingsystemmajrelease']}/${facts['architecture']}",
    enabled  => '1',
    gpgcheck => '1',
    gpgkey   => "http://xcat.org/files/xcat/repos/yum/${xcat::version}/xcat-dep/rh${facts['operatingsystemmajrelease']}/${facts['architecture']}/repodata/repomd.xml.key",
    name     => 'xcat-dep',
  }
}
