# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include xcat::server::service
class xcat::server::service {
  include xcat::server::install
  Class['xcat::server::install'] ~> Class['xcat::server::service']

  service { $xcat::server::service_name:
    ensure     => 'running',
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
  }
}
