# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include xcat::server::postgresql
class xcat::server::postgresql {
  include xcat
  include xcat::server

  if $xcat::server::dbengine == 'Pg' {
    package { 'perl-DBD-Pg':
      ensure => installed,
    }

    # xCAT database configuration file for PostgreSQL
    file { '/etc/xcat/cfgloc':
      ensure  => present,
      content => "Pg:dbname=${xcat::server::dbname};host=127.0.0.1|xcatadm|${xcat::server::dbpassword}",
      #content => "Pg:dbname=${xcat::server::dbname};host=${xcat::site_master}|xcatadm|${xcat::server::dbpassword}",
      group   => 'root',
      mode    => '0600',
      owner   => 'root',
      notify  => [
        Exec['dumpxCATdb -p /tmp/xcat-dbback'],
        Exec['restorexCATdb -p /tmp/xcat-dbback'],
        Class['xcat::server::service']
      ],
    }

    #exec { '/opt/xcat/bin/pgsqlsetup':
    #  command     => '/opt/xcat/bin/pgsqlsetup -i -V',
    #  environment => [ 'XCATPGPW="cluster"', ],
    #  onlyif      => '/usr/bin/test `/opt/xcat/bin/lsxcatd -a |sed -n "s/^dbengine=\(\S\)/\1/p"` == "SQLite"',
    #}

    # Backup xCAT SQlite database and restore into PostgreSQL
    #environment => [ 'XCATBYPASS=1', 'XCATCFG="/etc/xcat/cfgloc-puppet"', ],
    exec { 'dumpxCATdb -p /tmp/xcat-dbback':
      creates     => '/tmp/xcat-dbback',
      environment => 'XCATBYPASS=1',
      path        => '/opt/xcat/sbin:/usr/bin',
      refreshonly => true,
    } ->
    exec { 'restorexCATdb -p /tmp/xcat-dbback':
      environment => [ 'XCATBYPASS=1', ],
      notify      => Class['xcat::server::service'],
      path        => '/opt/xcat/sbin:/usr/bin',
      refreshonly => true,
    }
  }
}
