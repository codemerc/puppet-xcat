# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include xcat::server::install
class xcat::server::install {
  include xcat

  package { 'xCAT':
    ensure  => 'installed',
    require => [ Yumrepo['xcat-core'], Yumrepo['xcat-dep'], ],
  } ->
  exec { "chdef -t site master=${xcat::server::site_master_ip}":
    unless => "test `lsdef -c -t site -i master |sed 's/.*=\\(\\S*\\)/\\1/'` == '${xcat::server::site_master_ip}'",
    path   => '/opt/xcat/bin:/usr/bin',
  } ->
  exec { "chdef -t site nameservers=${xcat::server::site_nameservers}":
    unless => "test `lsdef -c -t site -i nameservers |sed 's/.*=\\(\\S*\\)/\\1/'` == '${xcat::server::site_nameservers}'",
    path   => '/opt/xcat/bin:/usr/bin',
  }
  # chtab key=system passwd.username=root passwd.password=`openssl passwd -1 abc123`
  exec { 'chtab key=system passwd.username=root passwd.password=`openssl passwd -1 abc123`':
    unless => 'tabdump -w key==system -w username==root passwd |grep -q "system.*root"',
    path   => '/opt/xcat/bin:/opt/xcat/sbin:/usr/bin',
  }
  # chtab key=ipmi passwd.username=ADMIN passwd.password=ADMIN
  # chtab key=idrac passwd.username=root passwd.password=calvin
  # makedns -n
  # makedhcp -n

  if false {
    file { '/tmp/go-xcat':
      ensure => 'file',
      mode   => '0500',
      source => 'https://raw.githubusercontent.com/xcat2/xcat-core/master/xCAT-server/share/xcat/tools/go-xcat',
    }

    exec { '/tmp/go-xcat':
      command => "/tmp/go-xcat --xcat-version='${xcat::version}' --yes install",
      creates => '/opt/xcat/share/xcat/tools/go-xcat',
      require => File['/tmp/go-xcat'],
    }
  }
}
