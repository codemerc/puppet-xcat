require 'spec_helper'
require 'puppet/type/xcatobj'

RSpec.describe 'the xcatobj type' do
  it 'loads' do
    expect(Puppet::Type.type(:xcatobj)).not_to be_nil
  end
end
